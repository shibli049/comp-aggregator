# Company Aggregator

## Goal

Goal​ of this project is to produce simple json files for each companies. Each json file​
contain all informations for the given company as json object. To handle millions of companies
information, mapreduce paradigm is used for the solution so that it can process lot of
data in adistributed system.

## Requirements

1. Maven version 3.
2. Jdk 8.

## How to run

* Maven is used as the build tool. You can run the project with:
    1. `mvn exec:java` default argument will be: `./src/main/resources/`.
    2. If you want to provide different location then: `mvn exec:java -Dexec.args="/path/to/your/directory/"`

* An output directory `output/System.currentTimeMillis()` directory will be created in this same directory.

* This output directory will contain every `$orgno.json` file created for every company found in given CSV files.

## Code Coverage Report

| Class%        | Method%       | Line%        |
| :-----------: |:-------------:| :-----------:|
| 100%          | 70%(44/62)    | 80%(183/227) |
