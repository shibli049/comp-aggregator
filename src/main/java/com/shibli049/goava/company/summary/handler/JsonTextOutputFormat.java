/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.handler;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputCommitter;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

/**
 * @author shibli
 */
public class JsonTextOutputFormat<KEYOUT, VALUEOUT> extends TextOutputFormat<KEYOUT, VALUEOUT> {

    /**
     * Generate a unique filename, based on the task id, name, and extension
     *
     * @param context   the task that is calling this
     * @param name      the base filename
     * @param extension the filename extension
     * @return a string like $name$extension
     */

    public synchronized static String getUniqueFile(TaskAttemptContext context,
                                                    String name, String extension) {
        if (extension == null || extension.isEmpty()) {
            extension = ".json";
        }
        String result = name + extension;
        return result;
    }

    /**
     * Get the default path and filename for the output format.
     *
     * @param context   the task context
     * @param extension an extension to add to the filename, default `.json`
     * @return a full path $output/_temporary/part$extension
     * @throws IOException
     */
    @Override
    public Path getDefaultWorkFile(TaskAttemptContext context,
                                   String extension) throws IOException {
        FileOutputCommitter c = (FileOutputCommitter) getOutputCommitter(context);
        return new Path(c.getWorkPath(),
                getUniqueFile(context, getOutputName(context), extension));
    }

}
