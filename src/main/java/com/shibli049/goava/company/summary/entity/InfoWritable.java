/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.entity;

import com.shibli049.goava.company.summary.handler.Utils;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author shibli
 */
// TODO: add project lombok
public class InfoWritable implements Writable {

    private BooleanWritable isCompany;
    private Text orgno;
    private Text field1;
    private Text field2;
    private Text field3;
    private Text field4;

    public InfoWritable() {
        setFieldsDefault();
    }


    public InfoWritable(String[] data, int maxColumnLimit, boolean isCompany) {
        this.isCompany = new BooleanWritable(isCompany);
        if (data == null || data.length < maxColumnLimit) {
            setFieldsDefault();
            return;
        }

        if (isCompany) {
            setCompanyInfo(data, maxColumnLimit);
        } else {
            setAccountInfo(data, maxColumnLimit);
        }

    }


    private void setFieldsDefault() {
        this.orgno = new Text();
        this.field1 = new Text();
        this.field2 = new Text();
        this.field3 = new Text();
        this.field4 = new Text();
        this.isCompany = new BooleanWritable();

    }

    //FIXME: order have to be changed when header orders changes.
    void setAccountInfo(String[] data, int maxColumnLimit) {
        if (data == null || data.length != maxColumnLimit) {
            return;
        }
        this.orgno = new Text(Utils.cleanString(data[1]));

        //DATE_FROM
        this.field1 = new Text(Utils.cleanString(data[2]));

        //DATE_TO
        this.field2 = new Text(Utils.cleanString(data[3]));

        //PROFIT_MARGIN_PERCENT
        this.field3 = new Text(Utils.cleanString(data[4]));

        //NET_OPERATING_INCOME
        this.field4 = new Text(Utils.cleanString(data[5]));
    }

    //FIXME: order have to be changed when header orders changes.
    void setCompanyInfo(String[] data,int maxColumnLimit) {
        if (data == null || data.length != maxColumnLimit) {
            return;
        }
        this.orgno = new Text(data[0]);

        //COMPANY_NAME
        this.field1 = new Text(Utils.cleanString(data[1]));

        //PHONE_NUMBER
        this.field2 = new Text(Utils.cleanString(data[2]));

        //SNI_CODE
        this.field3 = new Text(Utils.cleanString(data[3]));

        //SNI_TEXT
        this.field4 = new Text(Utils.cleanString(data[4]));
    }

    public BooleanWritable getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(BooleanWritable isCompany) {
        this.isCompany = isCompany;
    }

    public Text getOrgno() {
        return orgno;
    }

    public void setOrgno(Text orgno) {
        this.orgno = orgno;
    }

    public Text getField1() {
        return field1;
    }

    public void setField1(Text field1) {
        this.field1 = field1;
    }

    public Text getField2() {
        return field2;
    }

    public void setField2(Text field2) {
        this.field2 = field2;
    }

    public Text getField3() {
        return field3;
    }

    public void setField3(Text field3) {
        this.field3 = field3;
    }

    public Text getField4() {
        return field4;
    }

    public void setField4(Text field4) {
        this.field4 = field4;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        orgno.write(output);
        field1.write(output);
        field2.write(output);
        field3.write(output);
        field4.write(output);
        isCompany.write(output);
    }

    @Override
    public void readFields(DataInput input) throws IOException {

        orgno.readFields(input);
        field1.readFields(input);
        field2.readFields(input);
        field3.readFields(input);
        field4.readFields(input);
        isCompany.readFields(input);

    }

}
