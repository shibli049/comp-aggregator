/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.entity;

import com.google.gson.annotations.SerializedName;
import com.shibli049.goava.company.summary.handler.Utils;

/**
 * @author shibli
 */
// TODO: add project lombok
public class Account {

    @SerializedName("date_from")
    private String dateFrom;

    @SerializedName("date_to")
    private String dateTo;

    @SerializedName("profit_margin_percent")
    private Float profitMarginPercent;

    @SerializedName("net_operating_income")
    private Long netOperatingIncome;

    public Account() {

    }


    public Account(InfoWritable info) {
        if (!info.getIsCompany().get()) {
            this.dateFrom = Utils.parseString(info.getField1());
            this.dateTo = Utils.parseString(info.getField2());
            this.profitMarginPercent = Utils.parseFloat(info.getField3());
            this.netOperatingIncome = Utils.parseLong(info.getField4());
        }
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public Float getProfitMarginPercent() {
        return profitMarginPercent;
    }

    public void setProfitMarginPercent(Float profitMarginPercent) {
        this.profitMarginPercent = profitMarginPercent;
    }

    public Long getNetOperatingIncome() {
        return netOperatingIncome;
    }

    public void setNetOperatingIncome(Long netOperatingIncome) {
        this.netOperatingIncome = netOperatingIncome;
    }


}
