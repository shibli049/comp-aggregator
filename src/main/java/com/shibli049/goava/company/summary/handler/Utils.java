/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.handler;

import org.apache.hadoop.io.Text;

/**
 * @author shibli
 */
public class Utils {

    private Utils() {
    }

    public static String[] getRowValues(String row, int limit) {
        final String regex = "(?<!\\\\),";

        //FIXME: this have to be changed if csv column order changes
        if (row == null || row.isEmpty() || row.startsWith("ORG_NUMBER") || row.startsWith(",ORG_NUMBER")) {
            return null;
        }
        return row.split(regex, limit);
    }

    public static Long parseLong(Text value) {
        Long l = null;
        if(value != null) {
            try {
                l = Long.parseLong(value.toString());
            } catch (NumberFormatException ignored) {
            }
        }
        return l;
    }

    public static Float parseFloat(Text value) {
        Float f = null;
        if(value != null) {
            try {
                f = Float.parseFloat(value.toString());
            } catch (NumberFormatException ignored) {
            }
        }
        return f;
    }

    // if a string is empty it return null, so that the field won't go to json.
    public static String parseString(Text value) {
        String s = null;
        if (value != null && !value.toString().isEmpty()) {
            s = value.toString();
        }
        return s;
    }

    // replace " if a string start and end with it.
    public static String cleanString(String value) {
        if (value != null && value.length() > 1 && value.startsWith("\"") && value.endsWith("\"")) {
            value = value.substring(1, value.length() - 1);
        }
        return value;
    }

}
