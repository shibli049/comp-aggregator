/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.job;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shibli049.goava.company.summary.entity.Account;
import com.shibli049.goava.company.summary.entity.Company;
import com.shibli049.goava.company.summary.entity.InfoWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shibli
 */
public class InformationReducer extends Reducer<Text, InfoWritable, NullWritable, Text> {

    // MultipleOutputs to write a json for every key/orgno
    private MultipleOutputs<NullWritable, Text> mos;
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        mos = new MultipleOutputs<>(context);
    }


    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
        super.cleanup(context);
    }


    // write the company and account json to $orgno.json file
    @Override
    protected void reduce(Text orgno, Iterable<InfoWritable> infoWritables, Context context) throws IOException, InterruptedException {
        Company company = getCompanyInformation(orgno, infoWritables);
        if (company == null) return;
        mos.write(NullWritable.get(), new Text(gson.toJson(company)), orgno.toString());
    }


    /**
     * @param orgno
     * @param infoWritables
     * @return Company information
     */
    // reduce the orgno, infoWritables to company and accounts
    Company getCompanyInformation(Text orgno, Iterable<InfoWritable> infoWritables) {
        Company company = new Company(orgno);
        if(infoWritables != null) {
            List<Account> accounts = new ArrayList<>();
            for (InfoWritable info : infoWritables) {
                if (info.getIsCompany().get()) {
                    company.setCompanyDetail(info);
                } else {
                    Account account = new Account(info);
                    company.addAccount(account);
                }
            }
        }
        return company;
    }

}
