/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.entity;

import com.google.gson.annotations.SerializedName;
import com.shibli049.goava.company.summary.handler.Utils;
import org.apache.hadoop.io.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shibli
 */
// TODO: add project lombok
public class Company {


    @SerializedName("orgno")
    private Long orgNo;

    @SerializedName("company_name")
    private String companyName;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("sni_code")
    private Long sniCode;

    @SerializedName("sni_text")
    private String sniText;

    @SerializedName("accounts")
    private List<Account> accounts;

    public Company() {
    }

    public Company(Text orgno) {
        this.orgNo = Utils.parseLong(orgno);
    }

    public void setCompanyDetail(InfoWritable info) {
        if (info.getIsCompany().get()) {
            this.companyName = Utils.parseString(info.getField1());
            this.phoneNumber = Utils.parseString(info.getField2());
            this.sniCode = Utils.parseLong(info.getField3());
            this.sniText = Utils.parseString(info.getField4());
        }
    }

    public void addAccount(Account account) {
        if(account != null) {
            if (this.accounts == null) {
                this.accounts = new ArrayList<>();
            }
            accounts.add(account);
        }
    }

    public Long getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(Long orgNo) {
        this.orgNo = orgNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getSniCode() {
        return sniCode;
    }

    public void setSniCode(Long sniCode) {
        this.sniCode = sniCode;
    }

    public String getSniText() {
        return sniText;
    }

    public void setSniText(String sniText) {
        this.sniText = sniText;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }



}
