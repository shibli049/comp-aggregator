/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.job;

import com.shibli049.goava.company.summary.entity.InfoWritable;
import com.shibli049.goava.company.summary.handler.Utils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author shibli
 */
public class CompanyMap extends Mapper<LongWritable, Text, Text, InfoWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        InfoWritable info = getCompanyInfoFromRow(value);

        if (info != null) {
            context.write(info.getOrgno(), info);
        }
    }

    protected InfoWritable getCompanyInfoFromRow(Text value) {
        InfoWritable info = null;

        String line = value.toString();
        //FIXME: if the column number for companies.csv changes, this 5 have to be changed.
        // if the limit is not specified, then the trailing commas are ignored.
        int maxColumnLimit = 5;
        String data[] = Utils.getRowValues(line, maxColumnLimit);
        if (data != null && data.length == maxColumnLimit) {
            try {
                info = new InfoWritable(data, maxColumnLimit, true);
            } catch (Exception ignored) {
            }
        }
        return info;
    }


}
