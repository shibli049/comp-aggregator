/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.job;

import com.shibli049.goava.company.summary.entity.InfoWritable;
import com.shibli049.goava.company.summary.handler.JsonTextOutputFormat;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * @author shibli
 */
//TODO: logger should be added.
public class Main extends Configured implements Tool {

    // pass the directory where the csv files are located.
    // an output/System.currentTimeMillis() directory will be created in this directory.
    // where $orgno.json file will be created for every company found in given csv files.
    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new Main(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] args) throws Exception {

        // job default configuration
        Job job = Job.getInstance(getConf());
        job.setJobName("comp-aggregator");
        job.setJarByClass(Main.class);

        // base directory for input and output
        final String basePath = args[0];
        String compPathString = basePath + "/companies.csv";
        String accPathString = basePath + "/accounts.csv";
        Path compPath = new Path(compPathString);
        Path accPath = new Path(accPathString);

        // Mapper input configurations
        MultipleInputs.addInputPath(job, compPath, TextInputFormat.class, CompanyMap.class);
        MultipleInputs.addInputPath(job, accPath, TextInputFormat.class, AccountMap.class);


        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(InfoWritable.class);

        // reducer output base directory
        String outputDir = basePath + "/output/" + System.currentTimeMillis();
        job.getConfiguration().set(JsonTextOutputFormat.OUTDIR, outputDir);
        // tells the job to use JsonTextOutputFormat as the output formatter class
        LazyOutputFormat.setOutputFormatClass(job, JsonTextOutputFormat.class);

        // reducer output key and value class
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        //reducer
        job.setReducerClass(InformationReducer.class);

        // based on number of nodes, this can be changed for better performance.
        job.setNumReduceTasks(10);

        // if the job successfully runs, then return 0, else 1
        return job.waitForCompletion(true) ? 0 : 1;

    }

}
