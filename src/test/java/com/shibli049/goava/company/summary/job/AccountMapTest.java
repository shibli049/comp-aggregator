package com.shibli049.goava.company.summary.job;

import com.shibli049.goava.company.summary.entity.InfoWritable;
import org.apache.hadoop.io.Text;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class AccountMapTest {

    @Test
    public void getAccountInfoFromRow() {
        AccountMap accountMap = new AccountMap();

        System.out.println("accountMap.test.1");
        Text row = new Text(",ORG_NUMBER,DATE_FROM,DATE_TO,PROFIT_MARGIN_PERCENT,NET_OPERATING_INCOME");
        InfoWritable infoWritable = accountMap.getAccountInfoFromRow(row);
        assertNull(infoWritable);


        System.out.println("accountMap.test.2");
        row = new Text("0,5164060062,2016-01-01,2016-12-31,0.0,0");
        infoWritable = accountMap.getAccountInfoFromRow(row);
        assertNotNull(infoWritable);
        assertFalse(infoWritable.getIsCompany().get());
        assertEquals(infoWritable.getOrgno(), new Text("5164060062"));
        assertEquals(infoWritable.getField1(), new Text("2016-01-01"));
        assertEquals(infoWritable.getField2(), new Text("2016-12-31"));
        assertEquals(infoWritable.getField3(), new Text("0.0"));
        assertEquals(infoWritable.getField4(), new Text("0"));

    }
}