package com.shibli049.goava.company.summary.job;

import org.apache.hadoop.util.ToolRunner;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Ignore
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void mainTest() throws Exception {
        Main.main(null);
    }

    @Ignore
    @Test
    public void runTest() throws Exception {
        int exitCode = ToolRunner.run(new Main(), new String[]{"./src/main/resources/"});
        assertEquals(0, exitCode);
    }
}