package com.shibli049.goava.company.summary.handler;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class JsonTextOutputFormatTest {

    JsonTextOutputFormat jtof = new JsonTextOutputFormat();


    @Test
    public void getUniqueFile() {
        String result = jtof.getUniqueFile(null, "test", ".txt");
        assertEquals("test.txt", result);

        result = jtof.getUniqueFile(null, "test", "");
        assertEquals("test.json", result);

        result = jtof.getUniqueFile(null, "test", null);
        assertEquals("test.json", result);
    }
}