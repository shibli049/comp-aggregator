/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shibli049.goava.company.summary.handler;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author shibli
 */
public class UtilsTest {


    /**
     * Test of getRowValues method, of class Utils.
     */
    @Test
    public void testGetRowValues() {
        System.out.println("getRowValues");
        String row = "8024010178,STIFTELSEN REGNBÅGEN MED FIRMA REGNBÅGEN,,,";


        String[] results = Utils.getRowValues(row, 6);
        String result = String.join(",", results);

        assertEquals(row, result);
    }

    /**
     * Test of parseLong method, of class Utils.
     */
    @Test
    public void testParseLong() {
        System.out.println("parseLong");
        String value = "";
        Long expResult = null;
        Long result = Utils.parseLong(new Text(value));
        assertNull(result);
    }

    /**
     * Test of parseLong method, of class Utils.
     */
    @Test
    public void testParseLong2() {
        System.out.println("parseLong2");
        String value = "1234";
        Long expResult = 1234L;
        Long result = Utils.parseLong(new Text(value));
        assertEquals(expResult, result);
    }

    /**
     * Test of parseFloat method, of class Utils.
     */
    @Test
    public void testParseFloat() {
        System.out.println("parseFloat");
        String value = "";
        Float result = Utils.parseFloat(new Text(value));
        assertNull(result);
    }

    /**
     * Test of parseFloat method, of class Utils.
     */
    @Test
    public void testParseFloat2() {
        System.out.println("parseFloat2");
        String value = "2.5";
        Float expedtedResult = 2.5f;
        Float result = Utils.parseFloat(new Text(value));
        assertEquals(result, expedtedResult);
    }

    /**
     * Test of parseString method, of class Utils.
     */
    @Test
    public void testParseString() {
        System.out.println("parseString");
        String value = "";
        String result = Utils.parseString(new Text(value));
        assertNull(result);
    }

    /**
     * Test of parseString method, of class Utils.
     */
    @Test
    public void testParseString2() {
        System.out.println("parseString2");
        String value = "qweer";
        String expResult = "qweer";
        String result = Utils.parseString(new Text(value));
        assertEquals(expResult, result);
    }

    /**
     * Test of cleanString method, of class Utils.
     */
    @Test
    public void testCleanString() {
        System.out.println("cleanString");
        String value = "\"hello";
        String result = Utils.cleanString(value);
        assertEquals(value, result);
    }

    @Test
    public void testCleanString2() {
        System.out.println("cleanString2");
        String value = "\"hello\"";
        String expResult = "hello";
        String result = Utils.cleanString(value);
        assertEquals(expResult, result);
    }

    @Test
    public void testCleanString3() {
        System.out.println("cleanString3");
        String value = "hello\"";
        String result = Utils.cleanString(value);
        assertEquals(value, result);
    }


    @Test
    public void testCleanString4() {
        System.out.println("cleanString4");
        String value = "\"";
        String result = Utils.cleanString(value);
        assertEquals(value, result);
    }

    @Test
    public void testCleanString5() {
        System.out.println("cleanString5");
        String value = "";
        String result = Utils.cleanString(value);
        assertEquals(value, result);
    }

    @Test
    public void testCleanString6() {
        System.out.println("cleanString6");
        String result = Utils.cleanString(null);
        assertNull(result);
    }

}
