package com.shibli049.goava.company.summary.job;

import com.shibli049.goava.company.summary.entity.InfoWritable;
import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class CompanyMapTest {

    @Test
    public void getCompanyInfoFromRow() {
        CompanyMap companyMap = new CompanyMap();

        System.out.println("companyMap.test.1");
        Text row = new Text("ORG_NUMBER,COMPANY_NAME,PHONE_NUMBER,SNI_CODE,SNI_TEXT");
        InfoWritable infoWritable = companyMap.getCompanyInfoFromRow(row);
        assertNull(infoWritable);


        System.out.println("companyMap.test.2");
        row = new Text("3023562238,\"Mejer\\, Dennis Grönnegaard\",,2101,Skogsägare");
        infoWritable = companyMap.getCompanyInfoFromRow(row);
        assertNotNull(infoWritable);
        assertTrue(infoWritable.getIsCompany().get());
        assertEquals(infoWritable.getOrgno(), new Text("3023562238"));
        assertEquals(infoWritable.getField1(), new Text("Mejer\\, Dennis Grönnegaard"));
        assertEquals(infoWritable.getField2(), new Text(""));
        assertEquals(infoWritable.getField3(), new Text("2101"));
        assertEquals(infoWritable.getField4(), new Text("Skogsägare"));



        System.out.println("companyMap.test.3");
        row = new Text("5020688668,Tigercat Industries Inc,,9,Huvudnäring okänd");
        infoWritable = companyMap.getCompanyInfoFromRow(row);
        assertNotNull(infoWritable);
        assertTrue(infoWritable.getIsCompany().get());
        assertEquals(infoWritable.getOrgno(), new Text("5020688668"));
        assertEquals(infoWritable.getField1(), new Text("Tigercat Industries Inc"));
        assertEquals(infoWritable.getField2(), new Text(""));
        assertEquals(infoWritable.getField3(), new Text("9"));
        assertEquals(infoWritable.getField4(), new Text("Huvudnäring okänd"));



        System.out.println("companyMap.test.4");
        row = new Text("5568112634,,,9,Huvudnäring okänd");
        infoWritable = companyMap.getCompanyInfoFromRow(row);
        assertNotNull(infoWritable);
        assertTrue(infoWritable.getIsCompany().get());
        assertEquals(infoWritable.getOrgno(), new Text("5568112634"));
        assertEquals(infoWritable.getField1(), new Text(""));
        assertEquals(infoWritable.getField2(), new Text(""));
        assertEquals(infoWritable.getField3(), new Text("9"));
        assertEquals(infoWritable.getField4(), new Text("Huvudnäring okänd"));



        System.out.println("companyMap.test.5");
        row = new Text("7501314624,Anita Hedtjärn,073-5405553,96090,Övriga konsumenttjänstföretag");
        infoWritable = companyMap.getCompanyInfoFromRow(row);
        assertNotNull(infoWritable);
        assertTrue(infoWritable.getIsCompany().get());
        assertEquals(infoWritable.getOrgno(), new Text("7501314624"));
        assertEquals(infoWritable.getField1(), new Text("Anita Hedtjärn"));
        assertEquals(infoWritable.getField2(), new Text("073-5405553"));
        assertEquals(infoWritable.getField3(), new Text("96090"));
        assertEquals(infoWritable.getField4(), new Text("Övriga konsumenttjänstföretag"));



        System.out.println("companyMap.test.6");
        row = new Text("8024010178,STIFTELSEN REGNBÅGEN MED FIRMA REGNBÅGEN,,,");
        infoWritable = companyMap.getCompanyInfoFromRow(row);
        assertNotNull(infoWritable);
        assertTrue(infoWritable.getIsCompany().get());
        assertEquals(infoWritable.getOrgno(), new Text("8024010178"));
        assertEquals(infoWritable.getField1(), new Text("STIFTELSEN REGNBÅGEN MED FIRMA REGNBÅGEN"));
        assertEquals(infoWritable.getField2(), new Text(""));
        assertEquals(infoWritable.getField3(), new Text(""));
        assertEquals(infoWritable.getField4(), new Text(""));


    }
}