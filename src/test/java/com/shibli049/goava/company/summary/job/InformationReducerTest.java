package com.shibli049.goava.company.summary.job;

import com.shibli049.goava.company.summary.entity.Account;
import com.shibli049.goava.company.summary.entity.Company;
import com.shibli049.goava.company.summary.entity.InfoWritable;
import com.shibli049.goava.company.summary.handler.Utils;
import org.apache.hadoop.io.Text;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class InformationReducerTest {

    @Test
    public void getCompanyInformation() {
        InformationReducer informationReducer = new InformationReducer();
        System.out.println("reducer.test.1");
        Text orgno = new Text("10");
        Company company = informationReducer.getCompanyInformation(orgno,null);
        assertEquals(company.getOrgNo(), Utils.parseLong(orgno));
        assertNull(company.getCompanyName());
        assertNull(company.getAccounts());


        List<InfoWritable> infoList = new ArrayList<>();
        System.out.println("reducer.test.2");
        company = informationReducer.getCompanyInformation(orgno,infoList);
        assertEquals(company.getOrgNo(), Utils.parseLong(orgno));
        assertNull(company.getCompanyName());
        assertNull(company.getAccounts());


        System.out.println("reducer.test.3");
        String[] data = {"12345", "hello", "123444", "", ""};

        InfoWritable infoWritable = new InfoWritable(data, 5, true);
        orgno = infoWritable.getOrgno();
        infoList.add(infoWritable);
        company = informationReducer.getCompanyInformation(orgno,infoList);
        assertEquals(company.getOrgNo(), Utils.parseLong(orgno));
        assertEquals(data[1], company.getCompanyName());
        assertEquals(data[2], company.getPhoneNumber());
        assertNull(company.getAccounts());
        assertNull(company.getSniCode());
        assertNull(company.getSniText());


        System.out.println("reducer.test.4");
        data = new String[]{"1", "12345", "2015-01-01", "2015-12-31", "28.66", "9372"};

        infoWritable = new InfoWritable(data, 6, false);
        orgno = infoWritable.getOrgno();
        infoList.add(infoWritable);
        company = informationReducer.getCompanyInformation(orgno,infoList);
        assertEquals(company.getOrgNo(), Utils.parseLong(orgno));


        assertNotNull(company.getAccounts());
        assertTrue(company.getAccounts().size() == 1);
        Account account = company.getAccounts().get(0);

        assertEquals(data[2], account.getDateFrom());
        assertEquals(data[3], account.getDateTo());
        assertEquals((Float)Float.parseFloat(data[4]), account.getProfitMarginPercent());
        assertEquals((Long)Long.parseLong(data[5]), account.getNetOperatingIncome());

    }
}